/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "views/Dashboard.jsx";
import UserProfile from "views/UserProfile.jsx";
import TableList from "views/TableList.jsx";
import Typography from "views/Typography.jsx";
import Icons from "views/Icons.jsx";
import Maps from "views/Maps.jsx";
import Notifications from "views/Notifications.jsx";
import Upgrade from "views/Upgrade.jsx";
import ResultsDemonstration from "components/Results/ResultsDemonstration"
import HelpComponent from "components/Help/HelpComponent"
import Correlation from "components/Correlations/Correlation"
import ErrorDemonstration from "components/ErrorDemonstration/ErrorDemonstration"
import FileUpload from "components/FileUpload/FileUpload"
import BaselineRequest from "components/BaselineRequest/BaselineRequest"

const dashboardRoutes = [
  {
    path: "/fileUpload",
    name: "Upload file",
    icon: "pe-7s-cloud-upload",
    component: FileUpload,
    layout: "/admin"
  },
  {
    path: "/correlation",
    name: "Correlation",
    icon: "pe-7s-graph3",
    component: Correlation,
    layout: "/admin"
  },
  {
    path: "/baselineConfiguration",
    name: "Baseline Configuration",
    icon: "pe-7s-settings",
    component: BaselineRequest,
    layout: "/admin"
  }/*,
  {
    path: "/fileUploadComponent",
    name: "Upload file and save all baselines and errors",
    icon: "pe-7s-cloud-upload",
    component: FileUploadComponent,
    layout: "/admin"
  }*/,
  {
    path: "/results",
    name: "Results",
    icon: "pe-7s-display1",
    component: ResultsDemonstration,
    layout: "/admin"
  }
  ,
  {
    path: "/errorDemonstration",
    name: "Errors",
    icon: "pe-7s-graph2",
    component: ErrorDemonstration,
    layout: "/admin"
  },
  {
    path: "/help",
    name: "Help",
    icon: "pe-7s-study",
    component: HelpComponent,
    layout: "/admin"
  }/*
  ,
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "User Profile",
    icon: "pe-7s-user",
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/table",
    name: "Table List",
    icon: "pe-7s-note2",
    component: TableList,
    layout: "/admin"
  },/*
  {
    path: "/typography",
    name: "Typography",
    icon: "pe-7s-news-paper",
    component: Typography,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "pe-7s-science",
    component: Icons,
    layout: "/admin"
  },/*
  {
    path: "/maps",
    name: "Maps",
    icon: "pe-7s-map-marker",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "pe-7s-bell",
    component: Notifications,
    layout: "/admin"
  },
  {
    upgrade: true,
    path: "/upgrade",
    name: "Upgrade to PRO",
    icon: "pe-7s-rocket",
    component: Upgrade,
    layout: "/admin"
  }*/
];

export default dashboardRoutes;
