import React, { Component } from "react";
import { Card } from "components/Card/Card.jsx";
import { Grid, Row, Col } from "react-bootstrap";

export class HelpComponent extends Component {

    constructor() {
        super()
    }

    render() {
        return (
            <div>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="File Format"
                            content={
                                <div>
                                    There are 6 mandatory columns in the excel file to be uploaded, those columns are:
                                    <br></br>
                                    -Day - must be a number from 1 to 31
                                    <br></br>
                                    -Month - must be a number from 1 to 12
                                    <br></br>
                                    -Year - must be a number from 0 to 9999
                                    <br></br>
                                    -Time period - if there are 96 periods per day, the column must start from 1 (the beggining of a day) and go from 1-96
                                    <br></br>
                                    -Day of week - must be a number from 1 to 7
                                    <br></br>
                                    -A column with any name, that will represent the target values (usually consumption values) - the values must be numbers
                                    <br></br>
                                    <br></br>
                                    The excel can have other columns, so this web page allows you to write the desired target column, that could be any column except the first 5 referenced above
                                    <br></br>
                                    It's recommended that the excel file contains only one sheet
                                </div>
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="Correlation"
                            content={
                                <div>
                                    <div>Correlations will only be shown to the columns for which it is possible to be computed</div>
                                </div>
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="Results"
                            content={
                                <div>The results are saved on the server software, on the Results folder, with the date and time as its name</div>
                            }
                        />
                    </Col>
                </Row>
            </div>
        )
    }

}

export default HelpComponent