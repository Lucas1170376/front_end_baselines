import React from 'react';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';
import "components/Slider/Slider.css"

export default class Slider extends React.Component {

    constructor(props) {
        super(props);
     
        this.state = {
        };
      }
     
      render() {
        return (
          <InputRange
            maxValue={this.props.max}
            minValue={this.props.min}
            value={this.props.getValue()}
            onChange={value => this.props.changeValue(value)} />
        );
      }
}