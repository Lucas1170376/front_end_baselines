import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import CheckboxContainer from "components/Checkbox/CheckboxContainer"
import { Card } from "components/Card/Card.jsx";
import ChartComponent from "components/Chart/ChartComponent"
import ErrorsPopup from "components/Popups/ErrorsPopup"
import "components/FileUpload/Upload.css"

export class Results extends Component {

  constructor(props) {
    super(props)

    this.state = {
      checkedYears: new Map(),
      checkedMonths: new Map(),
      checkedDays: new Map(),
      checkedDaysOfWeek: new Map(),
      checkedTimePeriods: new Map(),
      series: null,
      options: null,
      isShowingGraph: false,
      showErrorsPopup: false,
      errorMatrix: null
    }
    this.isShowingGraph = this.isShowingGraph.bind(this)
    this.setIsShowingGraph = this.setIsShowingGraph.bind(this)
    this.getPeriodCheckboxes = this.getPeriodCheckboxes.bind(this)
    this.onChangeYears = this.onChangeYears.bind(this)
    this.onChangeMonths = this.onChangeMonths.bind(this)
    this.onChangeDays = this.onChangeDays.bind(this)
    this.onChangeDaysOfWeek = this.onChangeDaysOfWeek.bind(this)
    this.onChangeTimePeriods = this.onChangeTimePeriods.bind(this)
    this.onShowResultsButton = this.onShowResultsButton.bind(this)
    this.getGraphOptions = this.getGraphOptions.bind(this)
    this.toggleErrorsPopup = this.toggleErrorsPopup.bind(this)
    this.getErrors = this.getErrors.bind(this)
  }

  getErrors() {
    return this.state.errorMatrix
  }

  isShowingGraph() {
    return this.state.isShowingGraph
  }

  setIsShowingGraph(trueOrFalse) {
    this.setState({ isShowingGraph: trueOrFalse })
  }

  render() {
    return (
      <div className="results">
        <Row>
          <Col lg={3} sm={12}>
            <Card
              title="Current uploaded file:"
              content={this.props.getFile() != null ? this.props.getFile().name : "No file has been uploaded yet"}
            />
            {/*String(this.props.getResults())*/}
            {/*String(this.props.getInfo())*/}
          </Col>
        </Row>
        {
          this.getPeriodCheckboxes() != null ?
            <div>
              <Row>
                <Col lg={2}>
                  <Card
                    title="Select year(s)"
                  />
                </Col>
                <Col sm={10}>
                  <CheckboxContainer checkedItems={this.state.checkedYears} onChange={this.onChangeYears} checkboxes={this.getYearCheckboxes()}></CheckboxContainer>
                </Col>
              </Row>

              <Row>
                <Col lg={2}>
                  <Card
                    title="Select month(s)"
                  />
                </Col>
                <Col sm={10}>
                  <CheckboxContainer checkedItems={this.state.checkedMonths} onChange={this.onChangeMonths} checkboxes={this.getMonthCheckboxes()}></CheckboxContainer>
                </Col>
              </Row>

              <Row>
                <Col lg={2}>
                  <Card
                    title="Select day(s)"
                  />
                </Col>
                <Col sm={10}>
                  <CheckboxContainer checkedItems={this.state.checkedDays} onChange={this.onChangeDays} checkboxes={this.getDayCheckboxes()}></CheckboxContainer>
                </Col>
              </Row>

              <Row>
                <Col lg={2}>
                  <Card
                    title="Select day(s) of the week"
                  />
                </Col>
                <Col sm={10}>
                  <CheckboxContainer checkedItems={this.state.checkedDaysOfWeek} onChange={this.onChangeDaysOfWeek} checkboxes={this.getWeekdayCheckboxes()}></CheckboxContainer>
                </Col>
              </Row>

              <Row>
                <Col lg={2}>
                  <Card
                    title="Select time period(s)"
                  />
                </Col>
                <Col sm={10}>
                  <CheckboxContainer checkedItems={this.state.checkedTimePeriods} onChange={this.onChangeTimePeriods} checkboxes={this.getPeriodCheckboxes()}></CheckboxContainer>
                </Col>
              </Row>

              <Row>
                <Col>
                  <button onClick={this.onShowResultsButton}>Show Baselines</button>
                </Col>
              </Row>

            </div>

            : null
        }

        {/*<ChartComponent setIsShowingGraph={this.setIsShowingGraph} series={this.state.series}></ChartComponent>*/}

        {this.state.isShowingGraph == true ?
          <div>
            <Row>
              <Col lg={100} sm={12}>
                <ChartComponent setIsShowingGraph={this.setIsShowingGraph} series={this.state.series} options={this.state.options} ></ChartComponent>
              </Col>
            </Row>
            {
              this.state.errorMatrix != null ?
                <Row>
                  <Col>
                    <Card
                      content={
                        <div>
                          {
                            <Card
                              content={
                                <div>
                                  <button onClick={this.toggleErrorsPopup}>Show error summary</button>
                                  {this.state.showErrorsPopup ?
                                    <ErrorsPopup getErrors={this.getErrors} closePopup={this.toggleErrorsPopup} />
                                    :
                                    false
                                  }
                                </div>
                              }
                            />
                          }
                        </div>
                      }
                    />
                  </Col>
                </Row>
                : null
            }

          </div>
          : null
        }

      </div>
    );
  }

  toggleErrorsPopup() {
    this.setState({
      showErrorsPopup: !this.state.showErrorsPopup
    })
  }

  onShowResultsButton() {
    this.setIsShowingGraph(false)
    this.setState({ errorMatrix: null })

    let yearsList = []
    for (let [year, isChecked] of this.state.checkedYears) {
      if (isChecked) {
        yearsList.push(+year)
      }
    }
    let monthsList = []
    for (let [month, isChecked] of this.state.checkedMonths) {
      if (isChecked) {
        monthsList.push(+month)
      }
    }
    let daysList = []
    for (let [day, isChecked] of this.state.checkedDays) {
      if (isChecked) {
        daysList.push(+day)
      }
    }
    let daysOfWeekList = []
    for (let [dayOfWeek, isChecked] of this.state.checkedDaysOfWeek) {
      if (isChecked) {
        daysOfWeekList.push(+dayOfWeek)
      }
    }
    let periodsList = []
    for (let [period, isChecked] of this.state.checkedTimePeriods) {
      if (isChecked) {
        periodsList.push(+period)
      }
    }

    let results = this.props.getResults()
    let baselinesMap = {}
    let errorMatrix = []
    let totalPeriods = 0
    let series = []
    let categories = []
    let y = []
    for (let year of yearsList) {
      for (let month of monthsList) {
        for (let day of daysList) {
          let monthObject = results[year][month]
          if (monthObject != undefined) {
            let dayObject = monthObject[day]
            if (dayObject != undefined) {
              let dayOfWeek = dayObject['Day of week']
              let periods = dayObject['Periods']
              if (daysOfWeekList.includes(dayOfWeek)) {
                for (let period of periods) {
                  let timePeriod = +period['Time Period']
                  if (periodsList.includes(timePeriod)) {
                    let baselines = period['Baselines'][0]
                    let realValue = period['Real Value']
                    if (!('realValue' in baselinesMap)) {
                      baselinesMap['realValue'] = []
                    }
                    baselinesMap['realValue'].push(realValue)
                    for (let i = 0; i < Object.keys(baselines).length; i++) {
                      let baseline = baselines[i]
                      let previousDays = baseline[0]
                      let method = baseline[1]
                      let baselineValue = baseline[2]
                      let mapString = "X-" + previousDays + ";" + method
                      if (!(mapString in baselinesMap)) {
                        baselinesMap[mapString] = []
                      }
                      baselinesMap[mapString].push(baselineValue)

                      if (errorMatrix.length == 0) {
                        for (let baselineToBeAdded of baselines) {
                          let previousDaysToBeAdded = baselineToBeAdded[0]
                          let methodToBeAdded = baselineToBeAdded[1]
                          errorMatrix.push([previousDaysToBeAdded, methodToBeAdded, 0, 999999, -1])
                        }
                      }
                      let error = Math.round((Math.abs(baselineValue - realValue) + Number.EPSILON) * 100) / 100
                      if(errorMatrix[i] != undefined){
                        if (error < errorMatrix[i][3]) {
                          errorMatrix[i][3] = error
                        }
                        if (error > errorMatrix[i][4]) {
                          errorMatrix[i][4] = error
                        }
                        errorMatrix[i][2] += error
  
                      }
                      //if (!(mapString in baselineErrorsMap)) {
                      //  baselineErrorsMap[mapString] = 0
                      //}
                      //baselineErrorsMap[mapString] += Math.abs(baselineValue - realValue)

                    }
                    let category = String(day) + "/" + String(month) + "-" + String(timePeriod)
                    if (!categories.includes(category)) {
                      categories.push(category)
                    }
                    totalPeriods += 1
                  }
                }
              }
            }
          }
        }
      }
    }

    categories.push('real')

    for (let category in categories) {
      y.push({
        title: {
          formatter: function (val) {
            return val
          }
        }
      })
    }

    Object.keys(baselinesMap).forEach((baselineType) => {
      series.push({
        name: baselineType,
        data: baselinesMap[baselineType]
      })
    })

    for (let i = 0; i < errorMatrix.length; i++) {
      errorMatrix[i][2] = errorMatrix[i][2] / totalPeriods
    }
    this.setState({ errorMatrix: errorMatrix })


    this.setState({ series: series })
    this.getGraphOptions(categories, y)

  }

  getGraphOptions(categories, y) {
    let options = {
      chart: {
        height: 9999,
        type: 'line',
        zoom: {
          enabled: true
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        width: [],
        curve: 'straight',
        dashArray: []
      },
      title: {
        text: 'Baselines',
        align: 'left'
      },
      legend: {
        tooltipHoverFormatter: function (val, opts) {
          //return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
        }
      },
      markers: {
        size: 0,
        hover: {
          sizeOffset: 4
        }
      },
      xaxis: {
        categories: categories,
      },
      tooltip: {
        y
      },
      grid: {
        borderColor: '#f1f1f1',
      }
    }
    this.setState({ options: options })
    this.setIsShowingGraph(true)
  }

  getYearCheckboxes() {
    let info = this.props.getInfo()
    if (info == null) {
      return null
    }
    let checkboxes = []
    let list = info['yearsList']
    for (let period in list) {
      checkboxes.push({
        name: String(list[period]),
        key: String(list[period]),
        label: String(list[period])
      })
    }
    return checkboxes
  }

  getMonthCheckboxes() {
    let info = this.props.getInfo()
    if (info == null) {
      return null
    }
    let checkboxes = []
    let list = info['monthsList']
    for (let period in list) {
      checkboxes.push({
        name: String(list[period]),
        key: String(list[period]),
        label: String(list[period])
      })
    }
    return checkboxes
  }

  getDayCheckboxes() {
    let info = this.props.getInfo()
    if (info == null) {
      return null
    }
    let checkboxes = []
    let list = info['daysList']
    for (let period in list) {
      checkboxes.push({
        name: String(list[period]),
        key: String(list[period]),
        label: String(list[period])
      })
    }
    return checkboxes
  }

  getWeekdayCheckboxes() {
    let checkboxes = [{
      name: '1',
      key: 1,
      label: 1
    }, {
      name: '2',
      key: 2,
      label: 2
    }, {
      name: '3',
      key: 3,
      label: 3
    }, {
      name: '4',
      key: 4,
      label: 4
    }, {
      name: '5',
      key: 5,
      label: 5
    }, {
      name: '6',
      key: 6,
      label: 6
    }, {
      name: '7',
      key: 7,
      label: 7
    },]
    return checkboxes
  }

  getPeriodCheckboxes() {
    let info = this.props.getInfo()
    if (info == null) {
      return null
    }
    let checkboxes = []
    let list = info['periodsList']
    for (let period in list) {
      checkboxes.push({
        name: String(list[period]),
        key: String(list[period]),
        label: String(list[period])
      })
    }
    return checkboxes
  }

  onChangeYears(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedYears: prevState.checkedYears.set(item, isChecked) }));
  }

  onChangeMonths(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedMonths: prevState.checkedMonths.set(item, isChecked) }));
  }

  onChangeDays(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedDays: prevState.checkedDays.set(item, isChecked) }));
  }

  onChangeDaysOfWeek(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedDaysOfWeek: prevState.checkedDaysOfWeek.set(item, isChecked) }));
  }

  onChangeTimePeriods(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState(prevState => ({ checkedTimePeriods: prevState.checkedTimePeriods.set(item, isChecked) }));
  }

}

export default Results