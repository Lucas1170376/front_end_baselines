import React, { Component, PropTypes } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import ChartComponent from "components/Chart/ChartComponent"
import Checkbox from "components/Checkbox/Checkbox";
import InputRange from 'react-input-range';
import "components/FileUpload/Upload.css"

export class ResultsDemonstration extends Component {

    constructor(props) {
        super(props)

        this.state = {
        }
        this.renderSliders = this.renderSliders.bind(this)
        this.renderGraph = this.renderGraph.bind(this)
        this.calculateResultsAndErrors = this.calculateResultsAndErrors.bind(this)
        this.prepareGraph = this.prepareGraph.bind(this)
        this.togglePreviousDayCheckbox = this.togglePreviousDayCheckbox.bind(this)
        this.toggleMethodCheckbox = this.toggleMethodCheckbox.bind(this)
    }

    render() {
        return (
            <div>
                {this.props.getResults() != null ?
                    <div>
                        <this.renderSliders></this.renderSliders>
                        <button onClick={this.calculateResultsAndErrors}>Visualize Results</button>
                        <br />
                        <br />
                        <this.renderGraph></this.renderGraph>
                    </div>
                    :
                    <div>No Results yet to be Demonstrated</div>}
            </div>
        );
    }

    renderGraph() {
        return (
            <div>
                {this.props.getGraphIsActive() == true ?
                    <ChartComponent series={this.props.getGraphSeries()} options={this.props.getGraphOptions()} ></ChartComponent>
                    :
                    null
                }
            </div>
        )
    }

    calculateResultsAndErrors() {
        this.props.setGraphIsActive(false)
        this.props.setDemonstrationErrorsIsActive(false)

        let previousDaysFilter = this.props.getPreviousDaySelectedCheckboxes()
        let methodsFilter = this.props.getMethodSelectedCheckboxes()

        console.log(previousDaysFilter)
        console.log(methodsFilter)

        let years = this.props.getSliderYears()
        let months = this.props.getSliderMonths()
        let daysOfWeek = this.props.getSliderDaysOfWeek()
        let days = this.props.getSliderDays()
        let periods = this.props.getSliderPeriods()

        let results = this.props.getResults()

        let categories = []
        let realValues = []
        categories.push('Real Value')
        let baselinesMap = {}
        let errorsMap = {}

        for (let result of results) {
            let year = result['Year']
            if (!(year >= years['min'] && year <= years['max'])) {
                continue
            }
            let month = result['Month']
            if (!(month >= months['min'] && month <= months['max'])) {
                continue
            }
            let dayOfWeek = result['Day of Week']
            if (!(dayOfWeek >= daysOfWeek['min'] && dayOfWeek <= daysOfWeek['max'])) {
                continue
            }
            let day = result['Day']
            if (!(day >= days['min'] && day <= days['max'])) {
                continue
            }
            let period = result['Time Period']
            if (!(period >= periods['min'] && period <= periods['max'])) {
                continue
            }
            let realValue = result['Real Value']
            realValues.push(realValue)

            let baselineResults = result['Results'][0]
            for (let baselineResult of baselineResults) {
                let previousDays = String(baselineResult[0])
                let method = baselineResult[1]
                if (previousDaysFilter.has(previousDays) && methodsFilter.has(method)) {
                    let baseline = baselineResult[2]
                    let mapString = String(previousDays) + ';' + String(method)
                    if (!(mapString in baselinesMap)) {
                        baselinesMap[mapString] = []
                    }
                    baselinesMap[mapString].push(baseline)

                    let error = Math.round((Math.abs(baseline - realValue) + Number.EPSILON) * 100) / 100
                    if (!(mapString in errorsMap)) {
                        errorsMap[mapString] = []
                    }
                    errorsMap[mapString].push(error)

                    let category = mapString
                    if (!categories.includes(category)) {
                        categories.push(category)
                    }
                }
            }
        }

        this.props.setDemonstrationErrors(errorsMap)

        this.prepareGraph(baselinesMap, categories, realValues)

    }

    prepareGraph(baselinesMap, categories, realValues) {

        let y = []
        for (let category in categories) {
            y.push({
                title: {
                    formatter: function (val) {
                        return val
                    }
                }
            })
        }

        let series = []
        series.push({
            name: 'Real Value',
            data: realValues,
            type: 'area'
        })
        Object.keys(baselinesMap).forEach((baselineType) => {
            series.push({
                name: baselineType,
                data: baselinesMap[baselineType],
                type: 'line'
            })
        })

        let options = {
            chart: {
                height: 9999,
                type: 'line',
                zoom: {
                    enabled: true
                },
            },
            fill: {
                type: 'solid',
                opacity: [0.35, 1],
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: [],
                curve: 'straight',
                dashArray: []
            },
            title: {
                text: 'Baselines',
                align: 'left'
            },
            legend: {
                tooltipHoverFormatter: function (val, opts) {
                    //return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
                }
            },
            markers: {
                size: 0,
                hover: {
                    sizeOffset: 4
                }
            },
            xaxis: {
                categories: [],
            },
            tooltip: {
                y
            },
            grid: {
                borderColor: '#f1f1f1',
            }
        }

        console.log(series)

        this.props.setGraphSeries(series)
        this.props.setGraphOptions(options)
        this.props.setGraphIsActive(true)

    }

    renderSliders() {
        let timeInfo = this.props.getTimeInfo()
        let yearInfo = timeInfo['Year']
        let monthInfo = timeInfo['Month']
        let dayOfWeekInfo = timeInfo['Day of Week']
        let dayInfo = timeInfo['Day']
        let periodInfo = timeInfo['Time Period']

        let resultInfo = this.props.getResultsInfo()
        let previousDays = resultInfo['previousDays']
        let methods = resultInfo['methods']

        return (
            <div>
                <Card
                    title={"Select Years"}
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={yearInfo['max']}
                            minValue={yearInfo['min']}
                            value={this.props.getSliderYears()}
                            onChange={value => this.props.setSliderYears(value)}
                        />
                    }
                />
                <Card
                    title={"Select Months"}
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={monthInfo['max']}
                            minValue={monthInfo['min']}
                            value={this.props.getSliderMonths()}
                            onChange={value => this.props.setSliderMonths(value)}
                        />
                    }
                />
                <Card
                    title={"Select Days of Week"}
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={dayOfWeekInfo['max']}
                            minValue={dayOfWeekInfo['min']}
                            value={this.props.getSliderDaysOfWeek()}
                            onChange={value => this.props.setSliderDaysOfWeek(value)}
                        />
                    }
                />
                <Card
                    title={"Select Days"}
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={dayInfo['max']}
                            minValue={dayInfo['min']}
                            value={this.props.getSliderDays()}
                            onChange={value => this.props.setSliderDays(value)}
                        />
                    }
                />
                <Card
                    title={"Select Time Periods"}
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={periodInfo['max']}
                            minValue={periodInfo['min']}
                            value={this.props.getSliderPeriods()}
                            onChange={value => this.props.setSliderPeriods(value)}
                        />
                    }
                />
                <Card
                    title="Filter Previous Days Used as Historical Data"
                    content={
                        <div>
                            {
                                Object.keys(previousDays).map((pd, index) => {
                                    let previousDay = previousDays[index]
                                    return (
                                        <Checkbox
                                            label={String(previousDay)}
                                            handleCheckboxChange={this.togglePreviousDayCheckbox}
                                            isChecked={this.props.getPreviousDaySelectedCheckboxes().has(String(previousDay))}
                                            key={previousDay}
                                        />
                                    )
                                })
                            }
                        </div>
                    }
                />
                <Card
                    title="Filter Methods of Baseline Calculations"
                    content={
                        <div>
                            {
                                Object.keys(methods).map((m, index) => {
                                    let method = methods[index]
                                    return (
                                        <Checkbox
                                            label={method}
                                            handleCheckboxChange={this.toggleMethodCheckbox}
                                            isChecked={this.props.getMethodSelectedCheckboxes().has(method)}
                                            key={method}
                                        />
                                    )
                                })
                            }
                        </div>
                    }
                />
            </div>

        )
    }

    togglePreviousDayCheckbox(previousDay) {
        if (this.props.getPreviousDaySelectedCheckboxes().has(previousDay)) {
            this.props.deletePreviousDaySelectedCheckbox(previousDay)
        } else {
            this.props.addPreviousDaySelectedCheckbox(previousDay)
        }
    }

    toggleMethodCheckbox(method) {
        if (this.props.getMethodSelectedCheckboxes().has(method)) {
            this.props.deleteMethodSelectedCheckbox(method)
        } else {
            this.props.addMethodSelectedCheckbox(method)
        }
    }

}

export default ResultsDemonstration