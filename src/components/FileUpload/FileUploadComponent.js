import React, { Component, useMemo } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { post } from 'axios';
import "components/FileUpload/Upload.css"
import ErrorsPopup from "components/Popups/ErrorsPopup"
import { Card } from "components/Card/Card.jsx";
import { useDropzone } from 'react-dropzone';

export class FileUploadComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      errors: null,
      file: null,
      fileUploadMessage: null,
      computeBaselineFor: null,
      target_column: null,
      message: null,
      results: null,
      showErrorsPopup: false,
      startDate: null,
      endDate: null,
      selectedDay: null,
      selectedMonth: null,
      selectedYear: null,
      selectedPeriods: null,
      selectedDateMessage: null
    }
    this.getErrors = this.getErrors.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.StyledDropzone = this.StyledDropzone.bind(this)
    this.renderServerResponse = this.renderServerResponse.bind(this)
    this.onChangeTargetColumn = this.onChangeTargetColumn.bind(this)
    this.handleSelectChange = this.handleSelectChange.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.requestSaveBaselines = this.requestSaveBaselines.bind(this)
    this.toggleErrorsPopup = this.toggleErrorsPopup.bind(this)
  }

  getErrors(){
    return this.state.errors
  }

  onChangeFile(e) {
    this.props.setFile(e.target.files[0])
    this.setState({ file: e.target.files[0] })
  }

  onChangeTargetColumn(e) {
    this.setState({ target_column: e.target.value })
    this.props.setTargetColumn(e.target.value)
  }

  handleSelectChange(e) {
    e.preventDefault()
    this.setState({ computeBaselineFor: e.target.value })
  }

  onFormSubmit(e) {
    e.preventDefault()
    if(this.state.file == null){
      this.setState({ message: "Please upload an excel file" })
      return
    }
    if (this.state.target_column == null) {
      this.setState({ message: "Please choose a target column" })
      return
    }
    this.setState({ errors: null })
    this.setState({ message: "Waiting for a response from the server" })

    this.requestSaveBaselines().then((response) => {
      console.log("RESPONSE FROM THE SERVER")
      console.log(response.data)
      this.setState({ message: "Server response: " + response.data.message })
      this.setState({ errors: response.data.errorMatrix })
      this.props.setResults(response.data.data)
      this.props.setInfo(response.data.info)
    })
  }

  renderServerResponse() {
    return <div>{this.state.message}</div>
  }

  requestSaveBaselines() {
    //todo if urls for deployment, research envs
    let url = 'http://127.0.0.1:8000/getAllBaselines/';
    switch (this.state.computeBaselineFor) {
      case "Every day":
        url = 'http://127.0.0.1:8000/getAllBaselines/';
        break;
      case "Weekdays":
        url = 'http://127.0.0.1:8000/getAllBaselinesWeekdays/';
        break;
      case "Weekends":
        url = 'http://127.0.0.1:8000/getAllBaselinesWeekends/';
        break;
    }
    const formData = new FormData();
    formData.append('targetColumn', this.state.target_column)
    formData.append('file', this.state.file)
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return post(url, formData, config)
  }

  render() {
    return (
      <div className="fileUpload">
        <Row>
          <Col lg={100} sm={12}>
            <Card
              title="Upload File"
              category="Upload an excel(.xlsx) file"
              content={
                <div>
                    <this.StyledDropzone></this.StyledDropzone>
                    <form onSubmit={this.onFormSubmit}>
                      <label htmlFor="target_column">Target column: </label>
                      <input type="text" id="target_column" name="target_column" onChange={this.onChangeTargetColumn} /><br />
                      <label htmlFor="options">Compute baseline for: </label>
                      <select id="options" name="options" onChange={this.handleSelectChange}>
                        <option value="Every day" defaultValue>Every day</option>
                        <option value="Weekdays">Weekdays</option>
                        <option value="Weekends">Weekends</option>
                      </select><br />
                      <button type="submit">Save baselines and errors</button>
                    </form>
                    <this.renderServerResponse />
                    {
                    this.state.errors != null ?
                      <Card
                        content={
                          <div>
                            <button onClick={this.toggleErrorsPopup}>Show error summary</button>
                            {this.state.showErrorsPopup ?
                              <ErrorsPopup getErrors={this.getErrors} closePopup={this.toggleErrorsPopup} />
                              :
                              false
                            }
                          </div>
                        }
                      />
                      : null
                  }
                </div>
              }
            />
          </Col>
        </Row>
      </div>
    );
  }

  toggleErrorsPopup() {
    this.setState({
      showErrorsPopup: !this.state.showErrorsPopup
    })
  }

  StyledDropzone(props) {
    const baseStyle = {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '20px',
      borderWidth: 2,
      borderRadius: 2,
      borderColor: '#eeeeee',
      borderStyle: 'dashed',
      backgroundColor: '#fafafa',
      color: '#bdbdbd',
      outline: 'none',
      transition: 'border .24s ease-in-out'
    };

    const activeStyle = {
      borderColor: '#2196f3'
    };

    const acceptStyle = {
      borderColor: '#00e676'
    };

    const rejectStyle = {
      borderColor: '#ff1744'
    };

    const { acceptedFiles, getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive } = useDropzone({

    });

    const style = useMemo(() => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {})
    }), [
      isDragActive,
      isDragReject
    ]);

    return (
      <section className="container">
        <div {...getRootProps({ className: 'dropzone', style })}>
          <input {...getInputProps()} onChange={this.onChangeFile} />
          <p>Drag 'n' drop some files here, or click to select files</p>
        </div>
        <aside>
          <h4>Selected file:</h4>
          <ul>
            {this.state.file != null ? (this.state.file.name) : null}
          </ul>
        </aside>
      </section>
    );
  }

}

export default FileUploadComponent