import React, { Component, useMemo, useCallback } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { useDropzone } from 'react-dropzone';
import "components/FileUpload/Upload.css"
import { post } from 'axios';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export class FileUpload extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
        this.StyledDropzone = this.StyledDropzone.bind(this)
        this.onFileUpload = this.onFileUpload.bind(this)
        this.onChangeFile = this.onChangeFile.bind(this)
        this.uploadFile = this.uploadFile.bind(this)
        this.onDropFile = this.onDropFile.bind(this)
        this.renderFileUploadMessage = this.renderFileUploadMessage.bind(this)
    }

    onDropFile(file){
        this.props.setFile(file[0])
    }

    onChangeFile(e) {
        this.props.setFile(e.target.files[0])
    }

    onFileUpload(e) {
        e.preventDefault()
        console.log("File Upload button pressed")
        if (this.props.getFile() == null) {
            this.props.setFileUploadMessage("Please upload an excel file")
            return
        }
        this.props.setFileUploadMessage("Waiting for the server response")
        this.uploadFile().then((response) => {
            console.log("RESPONSE FROM THE SERVER")
            console.log(response.data)
            let data = response.data
            let columns = data.columnList
            let info = data.info
            let pathToFile = data.pathToFile
            let availableMethods = data.availableMethods[0]
            this.props.setAvailableMethods(availableMethods)
            this.props.setColumns(columns)
            this.props.setInfo(info)
            this.props.setPath(pathToFile)
            this.props.setFileUploadMessage("Upload successful")
        }).catch(error => {
            this.props.setFileUploadMessage("Something went wrong")
        })
    }

    uploadFile() {
        let url = "http://127.0.0.1:8000/uploadFile/"
        let formData = new FormData()
        formData.append('file', this.props.getFile())
        let config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        return post(url, formData, config)
    }

    render() {
        return (<div>
            <br/>
            <Grid fluid>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="Upload file"
                            category="Upload an excel(.xlsx) file"
                            content={
                                <div>
                                    <this.StyledDropzone></this.StyledDropzone>
                                    <button onClick={this.onFileUpload}> Upload File </button>
                                    {this.props.getFileUploadMessage() != null ? <div>{this.props.getFileUploadMessage()}</div> : null}
                                </div>
                            }
                        />
                    </Col>
                </Row>
            </Grid>
        </div>)
    }

    //todo could use this but too smoll
    renderFileUploadMessage(){
        let fileUploadMessage = this.props.getFileUploadMessage()
        if (fileUploadMessage == null){
            return null
        }
        if(fileUploadMessage == "Please upload an excel file"){
            return (<MuiAlert variant="filled" severity="warning"> PLEASE UPLOAD AN EXCEL FILE </MuiAlert>)
        }
        return null
    }

    StyledDropzone(props) {
        const baseStyle = {
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: '20px',
            borderWidth: 2,
            borderRadius: 2,
            borderColor: '#eeeeee',
            borderStyle: 'dashed',
            backgroundColor: '#fafafa',
            color: '#bdbdbd',
            outline: 'none',
            transition: 'border .24s ease-in-out'
        };

        const activeStyle = {
            borderColor: '#2196f3'
        };

        const acceptStyle = {
            borderColor: '#00e676'
        };

        const rejectStyle = {
            borderColor: '#ff1744'
        };

        const onDrop = useCallback(acceptedFiles => {
            this.onDropFile(acceptedFiles)
        }, [])

        const { acceptedFiles, getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive } = useDropzone({
            onDrop
        });

        const style = useMemo(() => ({
            ...baseStyle,
            ...(isDragActive ? activeStyle : {}),
            ...(isDragAccept ? acceptStyle : {}),
            ...(isDragReject ? rejectStyle : {})
        }), [
            isDragActive,
            isDragReject
        ]);

        return (
            <section className="container">
                <div {...getRootProps({ className: 'dropzone', style })}>
                    <input {...getInputProps()} onChange={this.onChangeFile} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                </div>
                <aside>
                    <h4>Selected file:</h4>
                    <ul>
                        {this.props.getFile() != null ? (this.props.getFile().name) : null}
                    </ul>
                </aside>
            </section>
        );
    }

}

export default FileUpload