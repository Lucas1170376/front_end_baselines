import Chart from "react-apexcharts";
import React, { Component } from "react";

class ChartComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }



    render() {
        return (
            <div id="chart">
                <Chart
                    options={this.props.options}
                    series={this.props.series}
                    type="line"
                    width="1000"
                    height="800"
                />
            </div>
        );
    }
}

export default ChartComponent