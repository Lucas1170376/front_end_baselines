import React, { Component, useMemo } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { post } from 'axios';
import InputRange from 'react-input-range';
import "components/Slider/Slider.css"
import Checkbox from "components/Checkbox/Checkbox";

export class BaselineRequest extends Component {

    constructor(props) {
        super(props)
        this.state = {
            yearValues: null,
            monthValues: null,
            dayOfWeekValues: null,
            dayValues: null,
            periodValues: null,
            targetColumn: null
        }
        this.exportToCsv = this.exportToCsv.bind(this)
        this.renderSliders = this.renderSliders.bind(this)
        this.renderDefaultSliders = this.renderDefaultSliders.bind(this)
        this.onSliderChange = this.onSliderChange.bind(this)
        this.renderAditionalSliders = this.renderAditionalSliders.bind(this)
        this.onRequestBaselines = this.onRequestBaselines.bind(this)
        this.requestBaselines = this.requestBaselines.bind(this)
        this.onChangeWeekdays = this.onChangeWeekdays.bind(this)
        this.onChangeTargetColumn = this.onChangeTargetColumn.bind(this)
        this.onRequestDownload = this.onRequestDownload.bind(this)
        this.requestDownload = this.requestDownload.bind(this)
        this.renderTargetColumnSelect = this.renderTargetColumnSelect.bind(this)
        this.toggleMethodCheckbox = this.toggleMethodCheckbox.bind(this)
    }

    onChangeWeekdays(e) {
        this.props.setTargetWeekdays(e.target.value)
    }

    onChangeTargetColumn(e) {
        this.setState({ targetColumn: e.target.value })
        this.props.setTargetColumn(e.target.value)
    }

    exportToCsv(Results) {
        var CsvString = "";
        CsvString += 'Year;Month;Day of Week;Day;Period;Previous Days;Method;Baseline;Real Value' + "\r\n"
        Results.forEach(function (RowItem, RowIndex) {
            RowItem.forEach(function (ColItem, ColIndex) {
                CsvString += ColItem + ';';
            });
            CsvString += "\r\n";
        });
        CsvString = "data:application/csv," + encodeURIComponent(CsvString);
        var x = document.createElement("A");
        x.setAttribute("href", CsvString);
        x.setAttribute("download", "results.csv");
        document.body.appendChild(x);
        x.click();
    }

    onRequestBaselines(e) {
        e.preventDefault()

        let requestData = {}
        let rangeOfVariation = {}
        requestData['Target Column'] = this.props.getTargetColumn()
        if (this.props.getTargetWeekdays() == "Every Day") {
            requestData['Target Days of Week'] = [1, 2, 3, 4, 5, 6, 7]
        } else if (this.props.getTargetWeekdays() == "Weekdays") {
            requestData['Target Days of Week'] = [1, 2, 3, 4, 5]
        } else if (this.props.getTargetWeekdays() == "Weekends") {
            requestData['Target Days of Week'] = [6, 7]
        } else {
            requestData['Target Days of Week'] = [1, 2, 3, 4, 5, 6, 7]
        }

        let columns = this.props.getColumns()

        Object.keys(columns).map((column, index) => {
            let columnName = columns[column]
            let percentage = this.state[columnName]
            rangeOfVariation[columnName] = percentage / 100
        })

        requestData['Path'] = this.props.getPath()
        requestData['Ranges of Variation'] = rangeOfVariation
        requestData['Methods'] = Array.from(this.props.getConfigurationMethodSelectedCheckboxes())

        if(Array.from(this.props.getConfigurationMethodSelectedCheckboxes()).length == 0){
            this.props.setFilteringMessage("Please select at least one method")
            return 
        }
 
        console.log('Request data')
        console.log(requestData)

        this.requestBaselines(requestData)
            .then((response) => {
                if (response.data.message != null) {
                    this.props.setFilteringMessage(response.data.message)
                } else {
                    this.props.setFilteringMessage('Baselines computed and returned succesfully')
                }
                console.log("Response from server")
                console.log(response.data)

                if ('message' in response.data) {
                    this.props.setFilteringMessage(response.data.message)
                    return
                }

                let resultsPath = response.data.resultsPath
                this.props.setResultsPath(resultsPath)

                let errors = response.data.errors
                this.props.setErrors(errors)

                let results = response.data.results
                this.props.setResults(results)

                let resultsInfo = response.data.resultsInfo
                this.props.setResultsInfo(resultsInfo)

                let timeInfo = response.data.timeInfo
                let yearInfo = timeInfo['Year']
                this.props.setSliderYears(yearInfo)
                let monthInfo = timeInfo['Month']
                this.props.setSliderMonths(monthInfo)
                let dayOfWeekInfo = timeInfo['Day of Week']
                this.props.setSliderDaysOfWeek(dayOfWeekInfo)
                let dayInfo = timeInfo['Day']
                this.props.setSliderDays(dayInfo)
                let periodInfo = timeInfo['Time Period']
                this.props.setSliderPeriods(periodInfo)
                this.props.setTimeInfo(timeInfo)

                this.props.setGraphIsActive(false)
                this.props.setOverallErrorsIsActive(false)
                this.props.setDemonstrationErrorsIsActive(false)

            }).catch(error => {
                this.props.setFilteringMessage('Something went wrong')
            })
    }

    requestBaselines(data) {
        this.props.setFilteringMessage('Waiting for server response')
        return post('http://127.0.0.1:8000/getRequestedBaselines/', data)
    }

    onRequestDownload(e) {
        let path = this.props.getResultsPath()
        e.preventDefault()
        this.requestDownload(path)
            .then((response) => {
                console.log(response.data)
                this.exportToCsv(response.data.data)
            })
    }

    requestDownload(path) {
        return post('http://127.0.0.1:8000/downloadResults/', { 'path': path })
    }

    render() {
        let availableMethods = this.props.getAvailableMethods()
        return (
            <Grid>
                <br />
                <Row>
                    <Col lg={100} sm={12}>
                        {
                            this.props.getColumns() != null ?
                                <div>
                                    <Card
                                        title="Choose the target column"
                                        content={
                                            <this.renderTargetColumnSelect></this.renderTargetColumnSelect>
                                        }
                                    />
                                    <Card
                                        title="Choose the target days of the week"
                                        content={
                                            <div>
                                                <label htmlFor="options">Select target days of the week : </label>
                                                <select id="options" name="options" onChange={this.onChangeWeekdays}>
                                                    <option value="Every day" defaultValue>Every day</option>
                                                    <option value="Weekdays">Weekdays</option>
                                                    <option value="Weekends">Weekends</option>
                                                </select>
                                            </div>
                                        }
                                    />
                                    <Card
                                        title="Filter Methods of Baseline Calculations"
                                        content={
                                            <div>
                                                {   
                                                    
                                                    Object.keys( Array.from(availableMethods)).map((m, index) => {
                                                        let method = availableMethods[m]
                                                        return (
                                                            <Checkbox
                                                                label={method}
                                                                handleCheckboxChange={this.toggleMethodCheckbox}
                                                                isChecked={this.props.getConfigurationMethodSelectedCheckboxes().has(method)}
                                                                key={method}
                                                            />
                                                        )
                                                    })
                                                }
                                            </div>
                                        }
                                    />
                                    <this.renderSliders></this.renderSliders>
                                    <Card
                                        title="Request baselines"
                                        content={
                                            <div>
                                                <button onClick={this.onRequestBaselines}> Request Baselines </button>
                                                {this.props.getFilteringMessage() != null ? <div> {this.props.getFilteringMessage()} </div> : null}
                                            </div>
                                        }
                                    />
                                    {
                                        this.props.getResultsPath() != null ?
                                            <Card
                                                title="Download the results and errors"
                                                content={
                                                    <div>
                                                        {
                                                            <button onClick={this.onRequestDownload}> Download Results </button>
                                                        }
                                                    </div>
                                                }
                                            />
                                            :
                                            null
                                    }
                                </div>
                                :
                                <div>Please upload a file in the File Upload Page</div>
                        }
                    </Col>
                </Row>
            </Grid>
        )
    }

    toggleMethodCheckbox(method) {
        if (this.props.getConfigurationMethodSelectedCheckboxes().has(method)) {
            this.props.deleteConfigurationMethodSelectedCheckbox(method)
        } else {
            this.props.addConfigurationMethodSelectedCheckbox(method)
        }
    }

    renderTargetColumnSelect() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }
        return (
            <div>
                {
                    Object.keys(columns).map((column, index) => {
                        let columnName = columns[index]
                        return (
                            <div>
                                <label>
                                    <input type="radio" value={columnName}
                                        checked={this.state.targetColumn === columnName}
                                        onChange={this.onChangeTargetColumn} />
                                    {columnName}
                                </label>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    renderSliders() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }
        return (
            <div>
                {
                    //? this is unused at the moment
                    /*<this.renderDefaultSliders></this.renderDefaultSliders>*/
                }
                <this.renderAditionalSliders></this.renderAditionalSliders>
            </div>
        )
    }

    renderAditionalSliders() {
        let columns = this.props.getColumns()
        return Object.keys(columns).map((column, index) => {
            let columnName = columns[column]
            let min = 0
            let max = 100
            if (this.state[columnName] == null) {
                this.setState({ [columnName]: max })
            }
            return (
                <Card
                    title={"Range of variance in column: " + String(columnName)}
                    content={
                        <InputRange
                            disabled={columnName === this.props.getTargetColumn()}
                            step={10}
                            formatLabel={value => `${value}%`}
                            maxValue={Math.ceil(max)}
                            minValue={Math.floor(min)}
                            value={this.state[columnName]}
                            onChange={value => this.onSliderChange(columnName, value)}
                        />
                    }
                />
            )
        })
    }

    //? this is unused at the moment
    renderDefaultSliders() {
        let info = this.props.getInfo()

        if (info == null) {
            return null
        }

        let yearList = info.yearList
        let monthList = info.monthList
        let dayOfWeekList = info.dayOfWeekList
        let dayList = info.dayList
        let timePeriodList = info.timePeriodList

        let firstYear = yearList[0]
        let lastYear = yearList[yearList.length - 1]
        let firstMonth = monthList[0]
        let lastMonth = monthList[monthList.length - 1]
        let firstDayOfWeek = dayOfWeekList[0]
        let lastDayOfWeek = dayOfWeekList[dayOfWeekList.length - 1]
        let firstDay = dayList[0]
        let lastDay = dayList[dayList.length - 1]
        let firstPeriod = timePeriodList[0]
        let lastPeriod = timePeriodList[timePeriodList.length - 1]

        if (this.state.yearValues == null) {
            this.setState({ yearValues: { min: firstYear, max: lastYear } })
        }

        if (this.state.monthValues == null) {
            this.setState({ monthValues: { min: firstMonth, max: lastMonth } })
        }

        if (this.state.dayOfWeekValues == null) {
            this.setState({ dayOfWeekValues: { min: firstDayOfWeek, max: lastDayOfWeek } })
        }
        if (this.state.dayValues == null) {
            this.setState({ dayValues: { min: firstDay, max: lastDay } })
        }
        if (this.state.periodValues == null) {
            this.setState({ periodValues: { min: firstPeriod, max: lastPeriod } })
        }

        return (
            <div>
                <Card
                    title="Select Years"
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={lastYear}
                            minValue={firstYear}
                            value={this.state.yearValues}
                            onChange={value => this.onSliderChange('yearValues', value)}
                        />
                    }
                />
                <Card
                    title="Select Months"
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={lastMonth}
                            minValue={firstMonth}
                            value={this.state.monthValues}
                            onChange={value => { this.onSliderChange('monthValues', value) }}
                        />
                    }
                />
                <Card
                    title="Select Days of the Week"
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={lastDayOfWeek}
                            minValue={firstDayOfWeek}
                            value={this.state.dayOfWeekValues}
                            onChange={value => { this.onSliderChange('dayOfWeekValues', value) }}
                        />
                    }
                />
                <Card
                    title="Select Days"
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={lastDay}
                            minValue={firstDay}
                            value={this.state.dayValues}
                            onChange={value => { this.onSliderChange('dayValues', value) }}
                        />
                    }
                />
                <Card
                    title="Select Periods"
                    content={
                        <InputRange
                            allowSameValues
                            maxValue={lastPeriod}
                            minValue={firstPeriod}
                            value={this.state.periodValues}
                            onChange={value => { this.onSliderChange('periodValues', value) }}
                        />
                    }
                />
            </div>
        )

    }

    onSliderChange(name, value) {
        this.setState({ [name]: value })
    }

}

export default BaselineRequest