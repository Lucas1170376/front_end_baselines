import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import "components/FileUpload/Upload.css"
import ReactEcharts from 'echarts-for-react';

export class CorrelationTable extends Component {

    constructor() {
        super()
        this.state = {
            isExpanded: false
        }
        this.toggleExpand = this.toggleExpand.bind(this)
        this.renderAllCorrelations = this.renderAllCorrelations.bind(this)
        this.renderTargetCorrelations = this.renderTargetCorrelations.bind(this)
    }

    toggleExpand() {
        this.setState({ isExpanded: !this.state.isExpanded })
    }

    render() {
        return (
            <div>
                <Card
                    title={"Correlation Results"}
                    content={
                        <this.renderAllCorrelations></this.renderAllCorrelations>
                    }
                />
            </div>
        )
    }

    render2() {
        return (
            <div>
                <button onClick={this.toggleExpand}> Show/Hide Correlations Details </button>
                {
                    this.state.isExpanded == true ?
                        <this.renderAllCorrelations></this.renderAllCorrelations>
                        :
                        <this.renderTargetCorrelations></this.renderTargetCorrelations>
                }
            </div>
        )
    }

    renderAllCorrelations() {
        let correlations = Array.from(this.props.getCorrelations())
        let columns = Array.from(correlations[0])

        let xAxis = columns
        let yAxis = columns

        //shift removes the first element of an array, in this case the column names 
        correlations.shift()

        for (let i = 0; i < correlations.length; i++) {
            for (let j = 0; j < correlations[i].length; j++) {
                if (correlations[i][j] == 1) {
                    correlations[i][j] = 'null'
                }
            }
        }

        let data = []
        let finalRow = 0
        for(let i = 0; i < correlations.length; i++){
            for(let j = 0; j < correlations[0].length; j++){
                data.push([i, j, correlations[i][j]])
            }
            finalRow = i
        }
        for(let j = 0; j < columns.length; j++){
            data.push([finalRow + 1, j, columns[j]])
        }

        data = data.map(function (item) {
            return [item[1], item[0], item[2] || '-'];
        });

        let option = {
            tooltip: {
                position: 'down'
            },
            animation: false,
            grid: {
                height: '100%',
                top: '0%'
            },
            xAxis: {
                type: 'category',
                data: xAxis,
                splitArea: {
                    show: true
                }
            },
            yAxis: {
                type: 'category',
                data: yAxis,
                splitArea: {
                    show: true
                }
            },
            visualMap: {
                min: -1,
                max: 1,
                calculable: true,
                orient: 'horizontal',
                left: 'center',
                bottom: '-20%'
            },
            series: [{
                name: 'Correlation Value',
                type: 'heatmap',
                data: data,
                label: {
                    show: true
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }]
        };
        return (
            <div className="popup">
                <div className="errorsPopupInner">
                    <ReactEcharts option={option} />
                </div>
            </div>
        )
    }

    renderTargetCorrelations() {
        let correlations = Array.from(this.props.getCorrelations())
        let columns = Array.from(correlations[0])

        let xAxis = columns
        let yAxis = columns

        //shift removes the first element of an array, in this case the column names 
        correlations.shift()

        for (let i = 0; i < correlations.length; i++) {
            for (let j = 0; j < correlations[i].length; j++) {
                if (correlations[i][j] == 99 || correlations[i][j] == 1) {
                    correlations[i][j] = 'null'
                }
            }
        }

        let data = []
        for(let i = 0; i < correlations.length; i++){
            for(let j = 0; j < 1; j++){
                data.push([i, j, correlations[i][j]])
            }
        }

        data = data.map(function (item) {
            return [item[1], item[0], item[2] || '-'];
        });

        let option = {
            tooltip: {
                position: 'down'
            },
            animation: false,
            grid: {
                height: '100%',
                top: '0%'
            },
            xAxis: {
                type: 'category',
                data: xAxis,
                splitArea: {
                    show: true
                }
            },
            yAxis: {
                type: 'category',
                data: yAxis,
                splitArea: {
                    show: true
                }
            },
            visualMap: {
                min: -1,
                max: 1,
                calculable: true,
                orient: 'horizontal',
                left: 'center',
                bottom: '-20%'
            },
            series: [{
                name: 'Correlation Value',
                type: 'heatmap',
                data: data,
                label: {
                    show: true
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }]
        };
        return (
            <div className="popup">
                <div className="errorsPopupInner">
                    <ReactEcharts option={option} />
                </div>
            </div>
        )
    }

    renderTargetCorrelations2() {
        let correlations = Array.from(this.props.getCorrelations())

        let columns = Array.from(correlations[0])
        //this adds a blank space to the first element of the columns, in order to get the diagonal blank space
        columns.unshift(' ')

        //shift removes the first element of an array, in this case the column names 
        correlations.shift()

        for (let i = 0; i < correlations.length; i++) {
            if (correlations[i][0] != columns[i + 1]) {
                correlations[i].unshift(columns[i + 1])
            }
            for (let j = 0; j < correlations[i].length; j++) {
                if (correlations[i][j] == 99) {
                    correlations[i][j] = 'null'
                }
            }
        }

        //only needs to show te first two columns
        let size = correlations.length
        correlations = correlations.map((val) => {
            return val.slice(0, -(size - 1))
        })
        columns.length = 2

        return (
            <div>
                <Card
                    title="Target Correlation Table"
                    ctTableFullWidth
                    ctTableResponsive
                    content={
                        <Table striped hover>
                            <thead>
                                <tr>
                                    {columns.map((prop, key) => {
                                        return <th key={key}>{prop}</th>
                                    })}
                                </tr>
                            </thead>
                            <tbody>
                                {correlations.map((prop, key) => {
                                    return (
                                        <tr key={key}>
                                            {prop.map((prop, key) => {
                                                return <td key={key}>{prop}</td>;
                                            })}
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </Table>
                    }
                />
            </div>
        )
    }

    renderAllCorrelations2() {
        let correlations = Array.from(this.props.getCorrelations())

        let columns = Array.from(correlations[0])
        //this adds a blank space to the first element of the columns, in order to get the diagonal blank space
        columns.unshift(' ')
        //shift removes the first element of an array, in this case the column names 
        correlations.shift()

        for (let i = 0; i < correlations.length; i++) {
            if (correlations[i][0] != columns[i + 1]) {
                correlations[i].unshift(columns[i + 1])
            }
            for (let j = 0; j < correlations[i].length; j++) {
                if (correlations[i][j] == 99) {
                    correlations[i][j] = 'null'
                }
            }
        }

        return (
            <div>
                <Card
                    title="Complete Correlation Table"
                    ctTableFullWidth
                    ctTableResponsive
                    content={
                        <Table striped hover>
                            <thead>
                                <tr>
                                    {columns.map((prop, key) => {
                                        return <th key={key}>{prop}</th>
                                    })}
                                </tr>
                            </thead>
                            <tbody>
                                {correlations.map((prop, key) => {
                                    return (
                                        <tr key={key}>
                                            {prop.map((prop, key) => {
                                                return <td key={key}>{prop}</td>;
                                            })}
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </Table>
                    }
                />
            </div>
        )

    }

}

export default CorrelationTable