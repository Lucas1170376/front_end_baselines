import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { post } from 'axios';
import "components/FileUpload/Upload.css"
import CorrelationTable from "components/Correlations/CorrelationTable"

export class Correlation extends Component {

    constructor() {
        super()
        this.state = {
            message: null,
            correlations: null,
        }
        this.onClickRequestCorrelation = this.onClickRequestCorrelation.bind(this)
        this.requestCorrelations = this.requestCorrelations.bind(this)
        this.getCorrelations = this.getCorrelations.bind(this)
    }

    getCorrelations() {
        console.log(this.state.correlations)
        return this.state.correlations
    }

    onClickRequestCorrelation(e) {
        e.preventDefault()
        let file = this.props.getFile()
        if (file == null) {
            this.setState({ message: "Please upload an excel file on the file upload tab" })
            return
        }
        this.state.correlations = null
        this.setState({ message: "Waiting for the server response" })
        this.requestCorrelations(file).then((response) => {
            console.log("RESPONSE FROM THE SERVER")
            console.log(response.data)
            this.setState({ message: response.data.message })
            this.setState({ correlations: response.data.correlations })

        })
    }

    requestCorrelations(file) {
        //todo research environments to change url on deployment
        let url = "http://127.0.0.1:8000/correlations/"
        let requestData = {}
        requestData['path'] = this.props.getPath()
        return post(url, requestData)
    }

    render() {
        return (
            <div>
                <br />
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Get all correlations of the columns from the uploaded file"
                                content={<div>
                                    <div>{this.props.getFile() != null ? this.props.getFile().name : "No file has been uploaded yet"} </div>
                                    <br></br>
                                    <br></br>
                                    <div>{this.props.getFile() != null ? <button onClick={this.onClickRequestCorrelation}>Request Correlation Info</button> : null} </div>
                                    {this.state.message != null ? <div>{this.state.message}</div> : null}
                                </div>}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            {
                                this.state.correlations != null ? <CorrelationTable getCorrelations={this.getCorrelations}></CorrelationTable> : null
                            }
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }

}

export default Correlation