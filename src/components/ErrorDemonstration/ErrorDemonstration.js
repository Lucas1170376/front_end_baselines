import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import "components/FileUpload/Upload.css"
import ReactEcharts from 'echarts-for-react';
import ErrorsTable from "components/ErrorDemonstration/ErrorsTable"

export class ErrorDemonstration extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }

        this.renderDemonstrationErrors = this.renderDemonstrationErrors.bind(this)
        this.renderOverallErrors = this.renderOverallErrors.bind(this)
        this.setDemonstrationOptions = this.setDemonstrationOptions.bind(this)
        this.setOverallOptions = this.setOverallOptions.bind(this)
    }

    render() {
        return (
            <Grid fluid>
                <br />
                <Row>
                    <Col md={8}>
                        <Card
                            title="Errors from the filtered results"
                            content={
                                <div>
                                    <button onClick={this.renderDemonstrationErrors}>Show Demonstration Errors</button>
                                    <br />
                                    {
                                        this.props.getDemonstrationErrorsIsActive() == true ?
                                            <div>
                                                <ReactEcharts option={this.props.getDemonstrationErrorsOptions()} />
                                            </div>
                                            :
                                            null
                                    }
                                </div>
                            }
                        />
                    </Col>
                    <Col md={4}>
                        <Card
                            title="Legend"
                            content={
                                <div>
                                    {
                                        this.props.getDemonstrationErrorsIsActive() == true ?
                                            <div>
                                                <ErrorsTable data={this.props.getDemonstrationLegend()} />
                                            </div>
                                            :
                                            null
                                    }
                                </div>
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col md={8}>
                        <Card
                            title="Errors from all the results requested from the server"
                            content={
                                <div>
                                    <button onClick={this.renderOverallErrors}>Show Overall Errors</button>
                                    <br />
                                    {
                                        this.props.getOverallErrorsIsActive() == true ?
                                            <div>
                                                <ReactEcharts option={this.props.getOverallErrorsOptions()} />
                                            </div>
                                            :
                                            null
                                    }
                                </div>
                            }
                        />
                    </Col>
                    <Col md={4}>
                        <Card
                            title="Legend"
                            content={
                                <div>
                                    {
                                        this.props.getOverallErrorsIsActive() == true ?
                                            <div>
                                                <ErrorsTable data={this.props.getOverallLegend()} />
                                            </div>
                                            :
                                            null
                                    }
                                </div>
                            }
                        />
                    </Col>
                </Row>
            </Grid>
        )
    }

    renderDemonstrationErrors() {
        let errors = this.props.getDemonstrationErrors()
        if (this.props.getDemonstrationErrorsIsActive() == true) {
            this.props.setDemonstrationErrorsIsActive(false)
        }
        if (errors == null) {
            return
        }
        let xAxisLegend = []
        let xAxisData = []
        let meanErrors = []
        let minErrors = []
        let maxErrors = []
        let baselineTypeCount = 1
        for (let baselineType in errors) {
            let baselineTypeSplit = baselineType.split(";")
            let previousDays = baselineTypeSplit[0]
            let method = baselineTypeSplit[1]
            xAxisLegend.push({ 'Number': baselineTypeCount, 'Previous Days': previousDays, 'Method': method })
            xAxisData.push(baselineTypeCount)
            let listOfErrors = errors[baselineType]
            let minError = 999999
            let maxError = -1
            let errorSum = 0
            for (let error of listOfErrors) {
                errorSum += error
                if (error < minError) {
                    minError = error
                }
                if (error > maxError) {
                    maxError = error
                }
            }
            let meanError = errorSum / listOfErrors.length
            meanErrors.push(meanError)
            minErrors.push(minError)
            maxErrors.push(maxError)
            baselineTypeCount += 1
        }
        this.props.setDemonstrationLegend(xAxisLegend)
        this.setDemonstrationOptions(meanErrors, minErrors, maxErrors, xAxisData)
    }

    renderOverallErrors() {
        let errors = this.props.getErrors()
        if (errors == null) {
            return
        }
        if (this.props.getOverallErrorsIsActive() == true) {
            this.props.setOverallErrorsIsActive(false)
        }
        let xAxisLegend = []
        let xAxisData = []
        let meanErrors = []
        let minErrors = []
        let maxErrors = []
        let baselineTypeCount = 1
        for (let baselineType in errors) {
            let baselineTypeSplit = baselineType.split(";")
            let previousDays = baselineTypeSplit[0]
            let method = baselineTypeSplit[1]
            xAxisLegend.push({ 'Number': baselineTypeCount, 'Previous Days': previousDays, 'Method': method })
            xAxisData.push(baselineTypeCount)
            let errorInfo = errors[baselineType]
            let errorCount = errorInfo['errorCount']
            if (errorCount == 0) {
                continue
            }
            let errorSum = errorInfo['errorSum']
            let maxError = errorInfo['maxError']
            let minError = errorInfo['minError']
            let meanError = errorSum / errorCount
            meanErrors.push(meanError)
            minErrors.push(minError)
            maxErrors.push(maxError)
            baselineTypeCount += 1
        }
        this.props.setOverallLegend(xAxisLegend)
        this.setOverallOptions(meanErrors, minErrors, maxErrors, xAxisData)
    }

    setDemonstrationOptions(meanErrors, minErrors, maxErrors, xAxisData) {
        let option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    //dataView: {show: true, readOnly: false},
                    //magicType: {show: true, type: ['line', 'bar']},
                    //restore: {show: true},
                    //saveAsImage: {show: true}
                }
            },
            legend: {
                data: ['Minimum Error', 'Maximum Error', 'Mean Error']
            },
            xAxis: [
                {
                    type: 'category',
                    data: xAxisData,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Minimum and Maximum Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                {
                    type: 'value',
                    name: 'Mean Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                }
            ],
            series: [
                {
                    name: 'Minimum Error',
                    type: 'bar',
                    data: minErrors
                },
                {
                    name: 'Maximum Error',
                    type: 'bar',
                    data: maxErrors
                },
                {
                    name: 'Mean Error',
                    type: 'line',
                    yAxisIndex: 1,
                    data: meanErrors
                }
            ]
        };
        this.props.setDemonstrationErrorsOptions(option)
        this.props.setDemonstrationErrorsIsActive(true)
    }

    setOverallOptions(meanErrors, minErrors, maxErrors, xAxisData) {
        let option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    //dataView: {show: true, readOnly: false},
                    //magicType: {show: true, type: ['line', 'bar']},
                    //restore: {show: true},
                    //saveAsImage: {show: true}
                }
            },
            legend: {
                data: ['Minimum Error', 'Maximum Error', 'Mean Error']
            },
            xAxis: [
                {
                    type: 'category',
                    data: xAxisData,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Minimum and Maximum Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                {
                    type: 'value',
                    name: 'Mean Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                }
            ],
            series: [
                {
                    name: 'Minimum Error',
                    type: 'bar',
                    data: minErrors
                },
                {
                    name: 'Maximum Error',
                    type: 'bar',
                    data: maxErrors
                },
                {
                    name: 'Mean Error',
                    type: 'line',
                    yAxisIndex: 1,
                    data: meanErrors
                }
            ]
        };
        this.props.setOverallErrorsOptions(option)
        this.props.setOverallErrorsIsActive(true)
    }

}

export default ErrorDemonstration