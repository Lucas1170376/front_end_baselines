import React, { Component } from 'react'
import ReactEcharts from 'echarts-for-react';

class ErrorsPopup extends Component {

    constructor(){
        super()
        this.state = {

        }
        this.getOptions = this.getOptions.bind(this)
    }

    render() {
        return (
            <div className="popup">
                <div className="errorsPopupInner">
                    <ReactEcharts option={this.getOptions()} />
                    <button onClick={this.props.closePopup}> Close </button>
                </div>
            </div>
        )
    }

    getOptions(){
        let errors = this.props.getErrors()
        let xAxisData = []
        let minErrorSeries = []
        let meanErrorSeries = []
        let maxErrorSeries = []
        
        for(let i = 0; i < errors.length; i++){
            let previousDays = errors[i][0]
            let method = errors[i][1]
            let xAxisDataPoint = "X-" + String(previousDays) + ";" + String(method)
            xAxisData.push(xAxisDataPoint)

            let meanError = errors[i][2]
            meanErrorSeries.push(meanError)

            let minError = errors[i][3]
            minErrorSeries.push(minError)

            let maxError = errors[i][4]
            maxErrorSeries.push(maxError)
        }

        let option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    //dataView: {show: true, readOnly: false},
                    //magicType: {show: true, type: ['line', 'bar']},
                    //restore: {show: true},
                    //saveAsImage: {show: true}
                }
            },
            legend: {
                data: ['Minimum Error', 'Maximum Error', 'Mean Error']
            },
            xAxis: [
                {
                    type: 'category',
                    data: xAxisData,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Minimum and Maximum Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                },
                {
                    type: 'value',
                    name: 'Mean Error',
                    axisLabel: {
                        formatter: '{value}'
                    }
                }
            ],
            series: [
                {
                    name: 'Minimum Error',
                    type: 'bar',
                    data: minErrorSeries
                },
                {
                    name: 'Maximum Error',
                    type: 'bar',
                    data: maxErrorSeries
                },
                {
                    name: 'Mean Error',
                    type: 'line',
                    yAxisIndex: 1,
                    data: meanErrorSeries
                }
            ]
        };

        return option

    }

}

export default ErrorsPopup