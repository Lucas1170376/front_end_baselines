/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import NotificationSystem from "react-notification-system";

import {Helmet} from "react-helmet";
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.jsx";

import { style } from "variables/Variables.jsx";

import routes from "routes.js";

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //file upload
      file: null,
      info: null,
      columns: null,
      path: null,
      fileUploadMessage: null,
      availableMethods: null,
      //correlations
      //filtering
      targetColumn: null,
      targetWeekDays: null,
      filteringMessage: null,
      resultsPath: null,
      errors: null,
      results: null,
      timeInfo: null,
      resultsInfo: null,
      //results
      previousDaySelectedCheckboxes: new Set(),
      methodsSelectedCheckboxes: new Set(),
      methodsSelectedCheckboxesConfiguration: new Set(),
      sliderYears: null,
      sliderMonths: null,
      sliderDaysOfWeek: null,
      sliderDays: null,
      sliderPeriods: null,
      graphIsActive: false,
      graphSeries: null,
      graphOptions: null,
      demonstrationErrors: null,
      //errors
      overallErrorsIsActive: false,
      demonstrationErrorsIsActive: false,
      overallErrorsOptions: null,
      demonstrationErrorsOptions: null,
      demonstrationLegend: null,
      overallLegend: null,
      //color
      color: 'green'
    };

    this.getFile = this.getFile.bind(this)
    this.setFile = this.setFile.bind(this)
    this.getInfo = this.getInfo.bind(this)
    this.setInfo = this.setInfo.bind(this)
    this.getPath = this.getPath.bind(this)
    this.setPath = this.setPath.bind(this)
    this.getColumns = this.getColumns.bind(this)
    this.setColumns = this.setColumns.bind(this)
    this.getFileUploadMessage = this.getFileUploadMessage.bind(this)
    this.setFileUploadMessage = this.setFileUploadMessage.bind(this)
    this.getTargetColumn = this.getTargetColumn.bind(this)
    this.setTargetColumn = this.setTargetColumn.bind(this)
    this.getTargetWeekdays = this.getTargetWeekdays.bind(this)
    this.setTargetWeekdays = this.setTargetWeekdays.bind(this)
    this.getFilteringMessage = this.getFilteringMessage.bind(this)
    this.setFilteringMessage = this.setFilteringMessage.bind(this)
    this.getResultsInfo = this.getResultsInfo.bind(this)
    this.setResultsInfo = this.setResultsInfo.bind(this)
    this.getResultsPath = this.getResultsPath.bind(this)
    this.setResultsPath = this.setResultsPath.bind(this)
    this.getErrors = this.getErrors.bind(this)
    this.setErrors = this.setErrors.bind(this)
    this.getResults = this.getResults.bind(this)
    this.setResults = this.setResults.bind(this)
    this.getTimeInfo = this.getTimeInfo.bind(this)
    this.setTimeInfo = this.setTimeInfo.bind(this)
    this.getSliderYears = this.getSliderYears.bind(this)
    this.setSliderYears = this.setSliderYears.bind(this)
    this.getSliderMonths = this.getSliderMonths.bind(this)
    this.setSliderMonths = this.setSliderMonths.bind(this)
    this.getSliderDaysOfWeek = this.getSliderDaysOfWeek.bind(this)
    this.setSliderDaysOfWeek = this.setSliderDaysOfWeek.bind(this)
    this.getSliderDays = this.getSliderDays.bind(this)
    this.setSliderDays = this.setSliderDays.bind(this)
    this.getSliderPeriods = this.getSliderPeriods.bind(this)
    this.setSliderPeriods = this.setSliderPeriods.bind(this)
    this.getGraphIsActive = this.getGraphIsActive.bind(this)
    this.setGraphIsActive = this.setGraphIsActive.bind(this)
    this.getGraphOptions = this.getGraphOptions.bind(this)
    this.setGraphOptions = this.setGraphOptions.bind(this)
    this.getGraphSeries = this.getGraphSeries.bind(this)
    this.setGraphSeries = this.setGraphSeries.bind(this)
    this.getDemonstrationErrors = this.getDemonstrationErrors.bind(this)
    this.setDemonstrationErrors = this.setDemonstrationErrors.bind(this)
    this.getOverallErrorsIsActive = this.getOverallErrorsIsActive.bind(this)
    this.setOverallErrorsIsActive = this.setOverallErrorsIsActive.bind(this)
    this.getDemonstrationErrorsIsActive = this.getDemonstrationErrorsIsActive.bind(this)
    this.setDemonstrationErrorsIsActive = this.setDemonstrationErrorsIsActive.bind(this)
    this.getOverallErrorsOptions = this.getOverallErrorsOptions.bind(this)
    this.setOverallErrorsOptions = this.setOverallErrorsOptions.bind(this)
    this.getDemonstrationErrorsOptions = this.getDemonstrationErrorsOptions.bind(this)
    this.setDemonstrationErrorsOptions = this.setDemonstrationErrorsOptions.bind(this)
    this.getOverallLegend = this.getOverallLegend.bind(this)
    this.setOverallLegend = this.setOverallLegend.bind(this)
    this.getDemonstrationLegend = this.getDemonstrationLegend.bind(this)
    this.setDemonstrationLegend = this.setDemonstrationLegend.bind(this)
    this.getPreviousDaySelectedCheckboxes = this.getPreviousDaySelectedCheckboxes.bind(this)
    this.deletePreviousDaySelectedCheckbox = this.deletePreviousDaySelectedCheckbox.bind(this)
    this.addPreviousDaySelectedCheckbox = this.addPreviousDaySelectedCheckbox.bind(this)
    this.getMethodSelectedCheckboxes = this.getMethodSelectedCheckboxes.bind(this)
    this.deleteMethodSelectedCheckbox = this.deleteMethodSelectedCheckbox.bind(this)
    this.addMethodSelectedCheckbox = this.addMethodSelectedCheckbox.bind(this)
    this.getConfigurationMethodSelectedCheckboxes = this.getConfigurationMethodSelectedCheckboxes.bind(this)
    this.deleteConfigurationMethodSelectedCheckbox = this.deleteConfigurationMethodSelectedCheckbox.bind(this)
    this.addConfigurationMethodSelectedCheckbox = this.addConfigurationMethodSelectedCheckbox.bind(this)
    this.getAvailableMethods = this.getAvailableMethods.bind(this)
    this.setAvailableMethods = this.setAvailableMethods.bind(this)
  }

  getFile() {
    return this.state.file
  }

  setFile(file) {
    this.setState({ file: file })
  }

  getInfo() {
    return this.state.info
  }

  setInfo(info) {
    this.setState({ info: info })
  }

  getColumns() {
    return this.state.columns
  }

  setColumns(columns) {
    this.setState({ columns: columns })
  }

  getPath() {
    return this.state.path
  }

  setPath(path) {
    this.setState({ path: path })
  }

  getFileUploadMessage() {
    return this.state.fileUploadMessage
  }

  setFileUploadMessage(fileUploadMessage) {
    this.setState({ fileUploadMessage: fileUploadMessage })
  }

  getTargetColumn() {
    return this.state.targetColumn
  }

  setTargetColumn(targetColumn) {
    this.setState({ targetColumn: targetColumn })
  }

  getTargetWeekdays() {
    return this.state.targetWeekDays
  }

  setTargetWeekdays(targetWeekDays) {
    this.setState({ targetWeekDays: targetWeekDays })
  }

  getFilteringMessage() {
    return this.state.filteringMessage
  }

  setFilteringMessage(filteringMessage) {
    this.setState({ filteringMessage: filteringMessage })
  }

  getResultsInfo(){
    return this.state.resultsInfo
  }

  setResultsInfo(resultsInfo){
    this.setState({ resultsInfo: resultsInfo })
  }

  getResultsPath() {
    return this.state.resultsPath
  }

  setResultsPath(resultsPath) {
    this.setState({ resultsPath: resultsPath })
  }

  getErrors() {
    return this.state.errors
  }

  setErrors(errors) {
    this.setState({ errors: errors })
  }

  getResults() {
    return this.state.results
  }

  setResults(results) {
    this.setState({ results: results })
  }

  getTimeInfo() {
    return this.state.timeInfo
  }

  setTimeInfo(timeInfo) {
    this.setState({ timeInfo: timeInfo })
  }

  getSliderYears() {
    return this.state.sliderYears
  }

  setSliderYears(sliderYears) {
    this.setState({ sliderYears: sliderYears })
  }

  getSliderMonths() {
    return this.state.sliderMonths
  }

  setSliderMonths(sliderMonths) {
    this.setState({ sliderMonths: sliderMonths })
  }

  getSliderDaysOfWeek() {
    return this.state.sliderDaysOfWeek
  }

  setSliderDaysOfWeek(sliderDaysOfWeek) {
    this.setState({ sliderDaysOfWeek: sliderDaysOfWeek })
  }

  getSliderDays() {
    return this.state.sliderDays
  }

  setSliderDays(sliderDays) {
    this.setState({ sliderDays: sliderDays })
  }

  getSliderPeriods() {
    return this.state.sliderPeriods
  }

  setSliderPeriods(sliderPeriods) {
    this.setState({ sliderPeriods: sliderPeriods })
  }

  getGraphIsActive() {
    return this.state.graphIsActive
  }

  setGraphIsActive(graphIsActive) {
    this.setState({ graphIsActive: graphIsActive })
  }

  getGraphSeries() {
    return this.state.graphSeries
  }

  setGraphSeries(graphSeries) {
    this.setState({ graphSeries: graphSeries })
  }

  getGraphOptions() {
    return this.state.graphOptions
  }

  setGraphOptions(graphOptions) {
    this.setState({ graphOptions: graphOptions })
  }

  getDemonstrationErrors() {
    return this.state.demonstrationErrors
  }

  setDemonstrationErrors(demonstrationErrors) {
    this.setState({ demonstrationErrors: demonstrationErrors })
  }

  getDemonstrationErrorsIsActive() {
    return this.state.demonstrationErrorsIsActive
  }

  setDemonstrationErrorsIsActive(demonstrationErrorsIsActive) {
    this.setState({ demonstrationErrorsIsActive: demonstrationErrorsIsActive })
  }

  getOverallErrorsIsActive() {
    return this.state.overallErrorsIsActive
  }

  setOverallErrorsIsActive(overallErrorsIsActive) {
    this.setState({ overallErrorsIsActive: overallErrorsIsActive })
  }

  getDemonstrationErrorsOptions() {
    return this.state.demonstrationErrorsOptions
  }

  setDemonstrationErrorsOptions(demonstrationErrorsOptions) {
    this.setState({ demonstrationErrorsOptions: demonstrationErrorsOptions })
  }

  getOverallErrorsOptions() {
    return this.state.overallErrorsOptions
  }

  setOverallErrorsOptions(overallErrorsOptions) {
    this.setState({ overallErrorsOptions: overallErrorsOptions })
  }

  getDemonstrationLegend() {
    return this.state.demonstrationLegend
  }

  setDemonstrationLegend(demonstrationLegend) {
    this.setState({ demonstrationLegend: demonstrationLegend })
  }

  getOverallLegend() {
    return this.state.overallLegend
  }

  setOverallLegend(overallLegend) {
    this.setState({ overallLegend: overallLegend })
  }

  getPreviousDaySelectedCheckboxes(){
    return this.state.previousDaySelectedCheckboxes
  }

  deletePreviousDaySelectedCheckbox(previousDay){
    this.state.previousDaySelectedCheckboxes.delete(previousDay)
  }

  addPreviousDaySelectedCheckbox(previousDay){
    this.state.previousDaySelectedCheckboxes.add(previousDay)
  }

  getMethodSelectedCheckboxes(){
    return this.state.methodsSelectedCheckboxes
  }

  deleteMethodSelectedCheckbox(method){
    this.state.methodsSelectedCheckboxes.delete(method)
  }

  addMethodSelectedCheckbox(method){
    this.state.methodsSelectedCheckboxes.add(method)
  }

  getAvailableMethods(){
    return this.state.availableMethods
  }

  setAvailableMethods(availableMethods){
    this.setState({ availableMethods: availableMethods })
  }

  getConfigurationMethodSelectedCheckboxes(){
    return this.state.methodsSelectedCheckboxesConfiguration
  }

  deleteConfigurationMethodSelectedCheckbox(method){
    this.state.methodsSelectedCheckboxesConfiguration.delete(method)
  }

  addConfigurationMethodSelectedCheckbox(method){
    this.state.methodsSelectedCheckboxesConfiguration.add(method)
  }

  handleNotificationClick = position => {
    var color = Math.floor(Math.random() * 4 + 1);
    var level;
    switch (color) {
      case 1:
        level = "success";
        break;
      case 2:
        level = "warning";
        break;
      case 3:
        level = "error";
        break;
      case 4:
        level = "info";
        break;
      default:
        break;
    }
  };

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        if (prop.path === "/fileUpload") {
          return (
            <Route
              path={prop.layout + prop.path}
              render={props => (
                <prop.component
                  {...props}
                  getFile={this.getFile}
                  setFile={this.setFile}
                  setColumns={this.setColumns}
                  setPath={this.setPath}
                  getFileUploadMessage={this.getFileUploadMessage}
                  setFileUploadMessage={this.setFileUploadMessage}
                  setInfo={this.setInfo}
                  handleClick={this.handleNotificationClick}
                  setAvailableMethods={this.setAvailableMethods}
                />
              )}
              key={key}
            />
          )
        } else
          if (prop.path === "/baselineConfiguration") {
            return (
              <Route
                path={prop.layout + prop.path}
                render={props => (
                  <prop.component
                    {...props}
                    getInfo={this.getInfo}
                    getFile={this.getFile}
                    getPath={this.getPath}
                    getTargetColumn={this.getTargetColumn}
                    setTargetColumn={this.setTargetColumn}
                    getColumns={this.getColumns}
                    getTargetWeekdays={this.getTargetWeekdays}
                    setTargetWeekdays={this.setTargetWeekdays}
                    getFilteringMessage={this.getFilteringMessage}
                    setFilteringMessage={this.setFilteringMessage}
                    getResultsPath={this.getResultsPath}
                    setResultsPath={this.setResultsPath}
                    setErrors={this.setErrors}
                    setResults={this.setResults}
                    setTimeInfo={this.setTimeInfo}
                    setSliderYears={this.setSliderYears}
                    setSliderMonths={this.setSliderMonths}
                    setSliderDaysOfWeek={this.setSliderDaysOfWeek}
                    setSliderDays={this.setSliderDays}
                    setSliderPeriods={this.setSliderPeriods}
                    setGraphIsActive={this.setGraphIsActive}
                    setOverallErrorsIsActive={this.setOverallErrorsIsActive}
                    setDemonstrationErrorsIsActive={this.setDemonstrationErrorsIsActive}
                    setResultsInfo={this.setResultsInfo}
                    getAvailableMethods={this.getAvailableMethods}
                    getConfigurationMethodSelectedCheckboxes={this.getConfigurationMethodSelectedCheckboxes}
                    deleteConfigurationMethodSelectedCheckbox={this.deleteConfigurationMethodSelectedCheckbox}
                    addConfigurationMethodSelectedCheckbox={this.addConfigurationMethodSelectedCheckbox}
                  />
                )}
                key={key}
              />
            )
          } else
            if (prop.path === "/results") {
              return (
                <Route
                  path={prop.layout + prop.path}
                  render={props => (
                    <prop.component
                      {...props}
                      getInfo={this.getInfo}
                      getFile={this.getFile}
                      getTargetColumn={this.getTargetColumn}
                      getColumns={this.getColumns}
                      getResults={this.getResults}
                      getTimeInfo={this.getTimeInfo}
                      getSliderYears={this.getSliderYears}
                      setSliderYears={this.setSliderYears}
                      getSliderMonths={this.getSliderMonths}
                      setSliderMonths={this.setSliderMonths}
                      getSliderDaysOfWeek={this.getSliderDaysOfWeek}
                      setSliderDaysOfWeek={this.setSliderDaysOfWeek}
                      getSliderDays={this.getSliderDays}
                      setSliderDays={this.setSliderDays}
                      getSliderPeriods={this.getSliderPeriods}
                      setSliderPeriods={this.setSliderPeriods}
                      getGraphIsActive={this.getGraphIsActive}
                      setGraphIsActive={this.setGraphIsActive}
                      getGraphSeries={this.getGraphSeries}
                      setGraphSeries={this.setGraphSeries}
                      getGraphOptions={this.getGraphOptions}
                      setGraphOptions={this.setGraphOptions}
                      setDemonstrationErrors={this.setDemonstrationErrors}
                      setDemonstrationErrorsIsActive={this.setDemonstrationErrorsIsActive}
                      getResultsInfo={this.getResultsInfo}
                      getPreviousDaySelectedCheckboxes={this.getPreviousDaySelectedCheckboxes}
                      deletePreviousDaySelectedCheckbox={this.deletePreviousDaySelectedCheckbox}
                      addPreviousDaySelectedCheckbox={this.addPreviousDaySelectedCheckbox}
                      getMethodSelectedCheckboxes={this.getMethodSelectedCheckboxes}
                      deleteMethodSelectedCheckbox={this.deleteMethodSelectedCheckbox}
                      addMethodSelectedCheckbox={this.addMethodSelectedCheckbox}
                    />
                  )}
                  key={key}
                />
              )
            } else
              if (prop.path === "/errorDemonstration") {
                return (
                  <Route
                    path={prop.layout + prop.path}
                    render={props => (
                      <prop.component
                        {...props}
                        getErrors={this.getErrors}
                        getDemonstrationErrors={this.getDemonstrationErrors}
                        getOverallErrorsIsActive={this.getOverallErrorsIsActive}
                        setOverallErrorsIsActive={this.setOverallErrorsIsActive}
                        getDemonstrationErrorsIsActive={this.getDemonstrationErrorsIsActive}
                        setDemonstrationErrorsIsActive={this.setDemonstrationErrorsIsActive}
                        getOverallErrorsOptions={this.getOverallErrorsOptions}
                        setOverallErrorsOptions={this.setOverallErrorsOptions}
                        getDemonstrationErrorsOptions={this.getDemonstrationErrorsOptions}
                        setDemonstrationErrorsOptions={this.setDemonstrationErrorsOptions}
                        getDemonstrationLegend={this.getDemonstrationLegend}
                        setDemonstrationLegend={this.setDemonstrationLegend}
                        getOverallLegend={this.getOverallLegend}
                        setOverallLegend={this.setOverallLegend}
                      />
                    )}
                    key={key}
                  />
                )
              }
        return (
          <Route
            path={prop.layout + prop.path}
            render={props => (
              <prop.component
                {...props}
                getInfo={this.getInfo}
                getResults={this.getResults}
                getFile={this.getFile}
                getColumns={this.getColumns}
                getPath={this.getPath}
              />
            )}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };

  //! DEFAULT CODE FROM THE TEMPLATE IS BELOW ////////////////////////////////////////////////////////////////////////

  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  handleImageClick = image => {
    this.setState({ image: image });
  };
  handleColorClick = color => {
    this.setState({ color: color });
  };
  handleHasImage = hasImage => {
    this.setState({ hasImage: hasImage });
  };
  handleFixedClick = () => {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show-dropdown open" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  };
  componentDidMount() {
    this.setState({ _notificationSystem: this.refs.notificationSystem });
    var _notificationSystem = this.refs.notificationSystem;
    var color = Math.floor(Math.random() * 4 + 1);
    var level;
    switch (color) {
      case 1:
        level = "success";
        break;
      case 2:
        level = "warning";
        break;
      case 3:
        level = "error";
        break;
      case 4:
        level = "info";
        break;
      default:
        break;
    }
    {/** 
    _notificationSystem.addNotification({
      title: <span data-notify="icon" className="pe-7s-gift" />,
      message: (
        <div>
          Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for
          every web developer.
        </div>
      ),
      level: level,
      position: "tr",
      autoDismiss: 15
    });
    */}
  }
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
    }
    if (e.history.action === "PUSH") {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
  }
  render() {
    return (
      <div className="wrapper">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Baselines</title>
          <link rel="canonical" href="http://mysite.com/example" />
        </Helmet>
        <NotificationSystem ref="notificationSystem" style={style} />
        <Sidebar {...this.props} routes={routes} image={this.state.image}
          color={this.state.color}
          hasImage={this.state.hasImage} />
        <div id="main-panel" className="main-panel" ref="mainPanel">
          <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          />
          <Switch>{this.getRoutes(routes)}</Switch>
          <Footer />
          {/** 
          <FixedPlugin
            handleImageClick={this.handleImageClick}
            handleColorClick={this.handleColorClick}
            handleHasImage={this.handleHasImage}
            bgColor={this.state["color"]}
            bgImage={this.state["image"]}
            mini={this.state["mini"]}
            handleFixedClick={this.handleFixedClick}
            fixedClasses={this.state.fixedClasses}
          />
          */}
        </div>
      </div>
    );
  }
}

export default Admin;
